const retrieveName = async (parametersJsonUrl) => {
  try {
    const response = await fetch(parametersJsonUrl);
    if (response.status === 200) {
      const allParams = await response.json();
      return allParams;
    } else {
      console.error(response);
      throw new Error(
        `Request did not complete successfully (code ${response.status})`
      );
    }
  } catch (error) {
    console.error(error);
  }
};

// Gets executed after page load
const enableGoals = function () {
  const disabledGoals = document.querySelectorAll(".bcpMenuItem.bcpDisabled");
  if (disabledGoals.length > 0) {
    disabledGoals.forEach((node) => node.classList.remove("bcpDisabled"));
  } else {
    console.warn("No disabled goals found...");
  }
};

// Gets remove saves auth failure cookies
const removeCookies = async function ({
  baseUrl,
  pluginType,
  pluginInstanceId,
}) {
  const parametersJsonUrl = `${baseUrl}/rest/${pluginType}s/${pluginInstanceId}?alt=json`;
  const params = await retrieveName(parametersJsonUrl);
  const pluginName = params?.[pluginType]?.pluginId || "";
  const cookies = `; ${document.cookie}`;
  const cookieNames = pluginName?.length ? cookies.split(`; `) : [];
  cookieNames.forEach((cookieName) => {
    if (cookieName?.startsWith(pluginName)) {
      console.warn("Deleting cookies for plugin => ", pluginName);
      document.cookie =
        cookieName + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
    }
  });
};

chrome.runtime.onMessage.addListener(function (message, sender, callback) {
  switch (message.functiontoInvoke) {
    case "enableGoals":
      enableGoals();
      break;
    case "removeCookies":
      removeCookies(message.arguments);
      break;
    default:
      console.warn(
        `Could not determine which function to invoke for "${message.functiontoInvoke}"`
      );
  }
});
