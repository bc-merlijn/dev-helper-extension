// Put all the javascript code here, that you want to execute in background.
const BLUECONIC_URL_PATTERNS = ["*://localhost/*", "*://*.blueconic.net/*"];
const URL_QUERY_REGEX =
    /^.*#id=([a-z0-9\-]+)(?:.*&type=[a-z]+&parents=([a-z]+)\-([a-z0-9\-]+)|&type=([a-z]+))/;

// Create context item for viewing parameters
chrome.contextMenus.create({
  id: "parent",
  title: "BlueConic Plugins development helper",
  contexts: ["all"],
  documentUrlPatterns: BLUECONIC_URL_PATTERNS,
});

// Create context item for enabling connection goals
chrome.contextMenus.create({
  id: "enableGoals",
  title: "Enable connection goals",
  parentId: "parent",
  contexts: ["all"],
  documentUrlPatterns: BLUECONIC_URL_PATTERNS,
});
// Create context item for enabling connection goals
chrome.contextMenus.create({
  id: "removeCookies",
  title: "Remove authentication cookies",
  parentId: "parent",
  contexts: ["all"],
  documentUrlPatterns: BLUECONIC_URL_PATTERNS,
});

// Create context item for enabling connection goals
chrome.contextMenus.create({
  id: "viewParameters",
  title: "View Plugin parameters",
  parentId: "parent",
  contexts: ["all"],
  documentUrlPatterns: BLUECONIC_URL_PATTERNS,
});

/**
 * Open a page that displays the parameters
 * @param {*} info
 * @param {*} tab
 */
function viewParameters(info, tab) {
  
  const urlItems = tab.url.match(URL_QUERY_REGEX);
  const targetSearchParams = new URLSearchParams({
    baseUrl: new URL(tab.url).origin,
    pluginType: urlItems[2] || urlItems[4],
    pluginInstanceId: urlItems[3] || urlItems[1],
    //goalId,
  });
  const targetUrl = `public/index.html?${targetSearchParams.toString()}`;

  chrome.tabs.create({
    index: tab.index + 1,
    url: chrome.runtime.getURL(targetUrl),
  });
}

/**
 * Send message to content script which in turn enables all goals on the page.
 * @param {*} info
 * @param {*} tab
 */
function enableGoals(info, tab) {
  if (tab && tab.id) {
    chrome.tabs.sendMessage(tab.id, {
      functiontoInvoke: "enableGoals",
    });
  } else {
    console.error(`Could not determine tab ${tab.id}`, tab);
  }
}
/**
 * Send message to content script which in turn enables all goals on the page.
 * @param {*} info
 * @param {*} tab
 */
function removeCookies(info, tab) {
  if (tab && tab.id) {
  const urlItems = tab.url.match(URL_QUERY_REGEX);
    chrome.tabs.sendMessage(tab.id, {
      functiontoInvoke: "removeCookies",
      arguments: {
        baseUrl: new URL(tab.url).origin,
        pluginType: urlItems[2] || urlItems[4],
        pluginInstanceId: urlItems[3] || urlItems[1],
      },
    });
  } else {
    console.error(`Could not determine tab ${tab.id}`, tab);
  }
}

chrome.contextMenus.onClicked.addListener(function(info, tab) {
  switch (info.menuItemId ) {
    case "enableGoals":
      enableGoals(info, tab);
      break;
    case "removeCookies":
      removeCookies(info, tab);
      break;
    case "viewParameters":
      viewParameters(info, tab);
      break;
  
    default:
      break;
  }
});
