const targetParameters = new URL(window.location.href).searchParams;
const baseUrl = targetParameters.get("baseUrl");
const pluginType = targetParameters.get("pluginType");
const pluginInstanceId = targetParameters.get("pluginInstanceId");
const parametersXmlUrl = `${baseUrl}/rest/${pluginType}s/${pluginInstanceId}`;
const parametersJsonUrl = `${parametersXmlUrl}?alt=json`;
const MAPPING_PROPERTIES = [
  "exportMapping",
  "exportMatching",
  "importMapping",
  "importMatching"
];
const mappingModelTypes = {
  NEW: "NEW",
  OLD: "OLD",
  MIXED: "MIXED",
  EMPTY: "EMPTY",
  UNKNOWN: "UNKNOWN"
};
const pluginLink = `${baseUrl}/blueconic/static/pages/main.html#id=${pluginInstanceId}&type=${pluginType}`;
export {
  targetParameters,
  baseUrl,
  pluginType,
  pluginInstanceId,
  parametersJsonUrl,
  parametersXmlUrl,
  pluginLink,
  MAPPING_PROPERTIES,
  mappingModelTypes
};
