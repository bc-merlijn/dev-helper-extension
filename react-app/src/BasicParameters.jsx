import React from "react";
import ParametersPrint from "./ParametersPrint";

const BasicParameters = ({pluginType, basicParameters, pluginParameters}) => {
  return (
    <>
      <h2>
        <i className="icon-tags"></i> Basic parameters
      </h2>
      <section id="sectionBasicParameters">
        <div id="parameters">
          <div id="parametersHeader"></div>
          <ParametersPrint
            parameters={
              basicParameters && pluginType === "connection"
                ? basicParameters
                : pluginParameters
            }
          />
        </div>
      </section>
    </>
  );
};

export default BasicParameters;
