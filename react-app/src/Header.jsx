import React from "react";

const Header = ({
  name,
  pluginLink,
  pluginInstanceId,
  lastModifiedDate,
  lastModifiedUser,
  parametersXmlUrl,
  parametersJsonUrl
}) => {
  return (
    <header>
      <div id="title">
        <img src="../../icons/blueconic-dev-helper.svg" alt="Logo" id="logo" />
        <a href={pluginLink} id="pluginLink">
          <h1 id="pluginName">{name}</h1>
        </a>
        <h1 id="pluginInstanceId">{pluginInstanceId}</h1>
      </div>
      <div>
        <div id="lastModified">
          <i>Last modified date </i>
          <span id="lastModifiedDate">{lastModifiedDate}</span>
          <i> by </i>
          <span id="lastModifiedUser">{lastModifiedUser?.user?.username}</span>
        </div>
        <nav>
          <span>RAW:</span>
          <a href={parametersXmlUrl} id="rawXml">
            XML
          </a>
          <a href={parametersJsonUrl} id="rawJson">
            JSON
          </a>
        </nav>
      </div>
    </header>
  );
};

export default Header;
