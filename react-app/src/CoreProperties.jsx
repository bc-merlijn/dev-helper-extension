import React, {useMemo} from "react";
import {MAPPING_PROPERTIES, pluginType, mappingModelTypes} from "./constants";
const {NEW, OLD, MIXED, EMPTY, UNKNOWN} = mappingModelTypes;

const CoreProperties = ({goalParameters, pluginId, pluginVersion}) => {
  // Detect mapping models
  const pluginMappingModel = useMemo(() => {
    const flattendMappings = [goalParameters]
      .flatMap(goal => {
        try {
          goal = JSON.parse(Array.isArray(goal) ? goal[0] : goal);
        } catch {
          goal = goal[0];
        }

        return MAPPING_PROPERTIES.map(mappingProperty => {
          if (goal?.parameters?.[mappingProperty]) {
            let mapping;
            try {
              mapping = JSON.parse(goal.parameters[mappingProperty]);
            } catch {
              mapping = goal.parameters[mappingProperty];
            }
            if (!mapping || (Array.isArray(mapping) && mapping.length === 0)) {
              return EMPTY;
            }

            if (
              Array.isArray(mapping.flat()[0].from.values) ||
              Array.isArray(mapping.flat()[0].to.values)
            ) {
              return NEW;
            } else {
              return OLD;
            }
          }
          if (goal?.state) {
            return NEW;
          } else {
            return EMPTY;
          }
        });
      })
      .flat();

    const couldNotDetermine =
      flattendMappings.length === 0 ||
      flattendMappings.every(model => model === EMPTY);
    if (couldNotDetermine) {
      return UNKNOWN;
    }
    const hasNew = flattendMappings.some(model => model === NEW);
    const hasOld = flattendMappings.some(model => model === OLD);

    if (hasOld && hasNew) {
      return MIXED;
    } else if (hasOld) {
      return OLD;
    } else if (hasNew) {
      return NEW;
    }
  }, [goalParameters]);

  const coreProperties = useMemo(
    () =>
      [
        {id: "pluginType", label: "Type", value: pluginType},
        {id: "pluginId", label: "Plugin ID", value: pluginId}
      ]
        .concat(
          pluginVersion
            ? [
                {
                  id: "pluginVersion",
                  label: "Installed plugin version",
                  value: pluginVersion
                }
              ]
            : []
        )
        .concat(
          pluginType === "connection"
            ? [
                {
                  id: "pluginMappingModel",
                  label: "Mapping model",
                  value: pluginMappingModel
                }
              ]
            : []
        ),
    [pluginId, pluginMappingModel, pluginVersion]
  );
  return (
    <>
      <h2>
        <i className="icon-dot-circled"></i> Core properties
      </h2>

      <section id="sectionCoreProperties">
        {coreProperties.map(
          ({id, label, value}) =>
            value &&
            value !== UNKNOWN && (
              <div className="coreProperty">
                <div className="corePropertyKey">{label}</div>
                <div
                  id={id}
                  className={
                    id === "pluginMappingModel" ? value.toLocaleLowerCase() : ""
                  }>
                  {value}
                </div>
              </div>
            )
        )}
      </section>
    </>
  );
};

export default CoreProperties;
