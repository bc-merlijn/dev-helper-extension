import React from "react";
import ParametersPrint from "./ParametersPrint";

const GoalParameters = ({goalParameters}) => {
  const arrayParameters = Array.isArray(goalParameters)
    ? goalParameters
    : [goalParameters];
  return (
    <>
      <h2>
        <i className="icon-flag-checkered"></i> Goal parameters
      </h2>
      <section id="sectionGoalParameters">
        <div id="goalParameters">
          {(arrayParameters?.length &&
            arrayParameters.map(goalParameter => {
              let goal;
              try {
                goal = JSON.parse(goalParameter);
              } catch {
                goal = goalParameter;
              }
              const {state, parameters, ...baseGoalParameters} = goal;
              const {id, name, type, categoryId, ...rest} = baseGoalParameters;
              return (
                <div id="goalTemplate" className="goal" key={type}>
                  <div className="goalHeader">
                    <div className={`goalType ${type || categoryId}`}>
                      <i className="icon-login" title="import"></i>
                      <i className="icon-logout" title="export"></i>
                    </div>
                    <div className="goalName">{name}</div>
                    <div className="goalId">{id}</div>
                  </div>
                  <div className="goalParameters">
                    {goal && <ParametersPrint parameters={rest} />}
                    {state && <ParametersPrint parameters={state} />}
                    {parameters && <ParametersPrint parameters={parameters} />}
                  </div>
                </div>
              );
            })) ||
            []}
        </div>
      </section>
    </>
  );
};

export default GoalParameters;
