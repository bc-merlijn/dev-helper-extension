import React from "react";
import {prettyPrintJson} from "pretty-print-json";

const ParametersPrint = ({parameters}) => {
  const isObject = value =>
    value && Object.getPrototypeOf(value) === Object.prototype;

  const isJson = value => {
    try {
      const parsed = JSON.parse(value);
      if (isObject(parsed) || Array.isArray(parsed)) {
        return true;
      }
    } catch (e) {
      return false;
    }
    return false;
  };

  const shouldPrintAsJson = value => {
    if (isObject(value)) {
      return true;
    }
    if (typeof value === "string") {
      return isJson(value);
    }
    if (Array.isArray(value)) {
      value = value.flat();
      return !!(
        value.length > 1 ||
        (!!value.length && value.every(item => isObject(item) || isJson(item)))
      );
    }
  };

  const printValue = value => {
    if (shouldPrintAsJson(value)) {
      try {
        const parsed = JSON.parse(value);
        if (isObject(parsed) || Array.isArray(parsed)) {
          return prettyPrintJson.toHtml(parsed);
        }
      } catch (e) {
        return prettyPrintJson.toHtml(value);
      }
    }
    return JSON.stringify(value);
  };

  return Object.entries(parameters)?.map(([key, value]) => (
    <div className="parameter" key={key}>
      <div className="parameterKey">{key}</div>
      <i
        className="icon-docs parameterCopy"
        onClick={() => navigator.clipboard.writeText(JSON.stringify(value))}
      />
      {value === undefined || value === null ? (
        <div className="empty">empty</div>
      ) : shouldPrintAsJson(value) ? (
        <pre
          className={`parameterValue ${shouldPrintAsJson(value) ? "json" : ""}`}
          dangerouslySetInnerHTML={{
            __html: printValue(value)
          }}
        />
      ) : (
        <div>{printValue(value)}</div>
      )}
    </div>
  ));
};

export default ParametersPrint;
