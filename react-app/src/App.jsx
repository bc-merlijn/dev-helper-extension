import React, {useEffect, useMemo, useState} from "react";

import "./App.css";
import Header from "./Header";
import CoreProperties from "./CoreProperties";
import BasicParameter from "./BasicParameters";
import GoalParameters from "./GoalParameters";
import {
  pluginType,
  pluginInstanceId,
  parametersJsonUrl,
  parametersXmlUrl,
  pluginLink
} from "./constants";

const App = () => {
  const [pluginParameters, setPluginParameters] = useState([]);
  const [pluginVersion, setPluginVersion] = useState("");

  useEffect(() => {
    const fetchParameters = async () => {
      try {
        const response = await fetch(parametersJsonUrl);
        if (response.status === 200) {
          const allParams = await response.json();
          setPluginParameters(allParams[pluginType]);
        } else {
          console.error(response);
          throw new Error(
            `Request did not complete successfully (code ${response.status})`
          );
        }
      } catch (error) {
        console.error(error);
      }
    };
    fetchParameters();
  }, []);

  const {name, lastModifiedDate, lastModifiedUser, pluginId} = pluginParameters;

  useEffect(() => {
    document.title = name;
  }, [name]);

  useEffect(() => {
    const fetchParameters = async () => {
      if (pluginId?.length) {
        const response = await fetch(
          `https://localhost/rest/plugins/${pluginId}?alt=json`
        );
        if (response.status === 200) {
          const pluginProperties = await response.json();
          setPluginVersion(pluginProperties?.plugin?.version || "");
        } else {
          console.error(response);
          throw new Error(
            `Request did not complete successfully (code ${response.status})`
          );
        }
      }
    };
    fetchParameters();
  }, [pluginId]);

  const innerParameters = useMemo(
    () =>
      Array.isArray(pluginParameters?.parameters?.parameter)
        ? pluginParameters?.parameters?.parameter
        : [pluginParameters?.parameters?.parameter],
    [pluginParameters?.parameters?.parameter]
  );

  const basicParameters = useMemo(() => {
    const parameters = {};
    if (innerParameters?.length) {
      Object.entries(
        innerParameters?.filter(param => {
          return param?.id !== "goalParameters";
        })
      )?.forEach(([key, value]) => {
        if (value && key) {
          parameters[value.id] = value?.values?.value;
        }
      });
    }
    return parameters;
  }, [innerParameters]);

  const goalParameters = useMemo(
    () =>
      innerParameters?.filter(param => param?.id === "goalParameters")?.[0]
        ?.values?.value || [],
    [innerParameters]
  );

  return (
    <>
      <Header
        name={name}
        pluginLink={pluginLink}
        pluginInstanceId={pluginInstanceId}
        lastModifiedDate={lastModifiedDate}
        lastModifiedUser={lastModifiedUser}
        parametersXmlUrl={parametersXmlUrl}
        parametersJsonUrl={parametersJsonUrl}
      />
      <CoreProperties
        goalParameters={goalParameters}
        pluginType={pluginType}
        pluginId={pluginId}
        pluginVersion={pluginVersion}
      />
      <BasicParameter
        pluginType={pluginType}
        basicParameters={basicParameters}
        pluginParameters={pluginParameters}
      />
      {!!goalParameters?.length && (
        <GoalParameters goalParameters={goalParameters} />
      )}
    </>
  );
};

export default App;
